//
//  OMBResidenceCreateConnection.h
//  OnMyBlock
//
//  Created by Tommy DANGerous on 12/16/13.
//  Copyright (c) 2013 OnMyBlock. All rights reserved.
//

#import "OMBConnection.h"

@class OMBResidence;

@interface OMBResidenceCreateConnection : OMBConnection

#pragma mark - Initializer

- (id) initWithResidence: (OMBResidence *) object;

@end
