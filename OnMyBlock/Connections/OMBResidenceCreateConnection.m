//
//  OMBResidenceCreateConnection.m
//  OnMyBlock
//
//  Created by Tommy DANGerous on 12/16/13.
//  Copyright (c) 2013 OnMyBlock. All rights reserved.
//

#import "OMBResidenceCreateConnection.h"

#import "Base64.h"
#import "OMBResidence.h"
#import "OMBResidenceImage.h"

@implementation OMBResidenceCreateConnection

#pragma mark - Initializer

- (id) initWithResidence: (OMBResidence *) object
{
  if (!(self = [super init])) return nil;

  NSString *string = [NSString stringWithFormat: 
    @"%@/places/create_listing_for_campus_founders", OnMyBlockAPIURL];
  NSURL *url = [NSURL URLWithString: string];
  NSMutableURLRequest *req = [NSMutableURLRequest requestWithURL: url];

  // Dates
  NSDateFormatter *dateFormatter = [NSDateFormatter new];
  dateFormatter.dateFormat = @"yyyy-MM-d";
  NSString *moveInDate = @"";
  if (object.moveInDate)
    moveInDate = [dateFormatter stringFromDate:
      [NSDate dateWithTimeIntervalSince1970: object.moveInDate]];
  NSString *moveOutDate = @"";
  if (object.moveOutDate)
    moveOutDate = [dateFormatter stringFromDate:
      [NSDate dateWithTimeIntervalSince1970: object.moveOutDate]];
  // Residence images
  NSMutableArray *residenceImages = [NSMutableArray array];
  for (OMBResidenceImage *residenceImage in [object imagesArray]) {
    // Convert our image to Base64 encoding
    [Base64 initialize];
    NSData *imageData = UIImagePNGRepresentation(residenceImage.image);
    if (residenceImage.image) {
      NSDictionary *dict = @{
        @"image_data": [Base64 encode: imageData],
        @"position":   [NSNumber numberWithInt: residenceImage.position]
      };
      [residenceImages addObject: dict];
    }
  }
  NSDictionary *objectParams = @{
    @"address":       object.address ? object.address : @"",
    @"amenities":     object.amenities ? [object amenitiesString] : @"",
    @"city":          object.city ? object.city : @"",
    @"desc":          object.description ? object.description : @"",
    @"email":         object.email ? object.email : @"",
    @"landlord_name": object.landlordName ? object.landlordName : @"",
    @"lease_months":  object.leaseMonths ? 
      [NSNumber numberWithInt: object.leaseMonths] : @"",
    @"min_bathrooms": object.bathrooms ? 
      [NSNumber numberWithFloat: object.bathrooms] : @"",
    @"min_bedrooms":  object.bedrooms ? 
      [NSNumber numberWithInt: object.bedrooms] : @"",
    @"min_rent":      object.rent ? 
      [NSNumber numberWithFloat: object.rent] : @"",
    @"move_in_date":  moveInDate,
    @"move_out_date": moveOutDate,
    @"neighborhood":  object.neighborhood ? object.neighborhood : @"",
    @"phone":         object.phone ? object.phone : @"",
    @"property_type": object.propertyType ? object.propertyType : @"",
    @"state":         object.state ? object.state : @"",
    @"zip":           object.zip ? object.zip : @""
  };
  NSDictionary *params = @{
    @"access_token":     [OMBUser currentUser].accessToken,
    @"residence_images": residenceImages,
    @"residence":        objectParams
  };
  NSData *json = [NSJSONSerialization dataWithJSONObject: params
    options: 0 error: nil];
  [req addValue: @"application/json" forHTTPHeaderField: @"Content-Type"];
  [req setHTTPBody: json];
  [req setHTTPMethod: @"POST"];
  self.request = req;
  
  return self;
}

#pragma mark - Protocol

#pragma mark - Protocol NSURLConnectionDataDelegate

- (void) connectionDidFinishLoading: (NSURLConnection *) connection
{
  NSDictionary *json = [NSJSONSerialization JSONObjectWithData: container
    options: 0 error: nil];
  NSLog(@"%@", json);
  [super connectionDidFinishLoading: connection];
}

@end
