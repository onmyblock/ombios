//
//  OMBResidenceImage.m
//  OnMyBlock
//
//  Created by Tommy DANGerous on 11/21/13.
//  Copyright (c) 2013 OnMyBlock. All rights reserved.
//

#import "OMBResidenceImage.h"

@implementation OMBResidenceImage

@synthesize absoluteString;
@synthesize image;
@synthesize position;

@end
