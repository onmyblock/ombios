//
//  OMBCreateListingDetailsViewController.h
//  OnMyBlock
//
//  Created by Tommy DANGerous on 12/13/13.
//  Copyright (c) 2013 OnMyBlock. All rights reserved.
//

#import "OMBRenterApplicationAddModelViewController.h"

@interface OMBCreateListingDetailsViewController : 
  OMBRenterApplicationAddModelViewController
<UIPickerViewDataSource, UIPickerViewDelegate>
{
  NSArray *propertyTypeArray;
  UIPickerView *propertyTypePickerView;
}

@property (nonatomic, strong) TextFieldPadding *bathTextField;
@property (nonatomic, strong) TextFieldPadding *bedTextField;
@property (nonatomic, strong) TextFieldPadding *leaseMonthsTextField;
@property (nonatomic, strong) TextFieldPadding *propertyTypeTextField;
@property (nonatomic, strong) TextFieldPadding *rentTextField;

@end
