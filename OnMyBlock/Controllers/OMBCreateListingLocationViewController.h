//
//  OMBCreateListingLocationViewController.h
//  OnMyBlock
//
//  Created by Tommy DANGerous on 12/13/13.
//  Copyright (c) 2013 OnMyBlock. All rights reserved.
//

#import "OMBRenterApplicationAddModelViewController.h"

@interface OMBCreateListingLocationViewController : 
  OMBRenterApplicationAddModelViewController
<CLLocationManagerDelegate, UIPickerViewDataSource, UIPickerViewDelegate>
{
  CLLocationManager *locationManager;
  NSArray *neighborhoodArray;
  UIPickerView *neighborhoodPickerView;
}

@property (nonatomic, strong) TextFieldPadding *addressTextField;
@property (nonatomic, strong) TextFieldPadding *cityTextField;
@property (nonatomic, strong) TextFieldPadding *neighborhoodTextField;
@property (nonatomic, strong) TextFieldPadding *stateTextField;
@property (nonatomic, strong) TextFieldPadding *unitTextField;
@property (nonatomic, strong) TextFieldPadding *zipTextField;

@end
