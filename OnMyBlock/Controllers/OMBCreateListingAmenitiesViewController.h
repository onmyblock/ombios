//
//  OMBCreateListingAmenitiesViewController.h
//  OnMyBlock
//
//  Created by Tommy DANGerous on 12/16/13.
//  Copyright (c) 2013 OnMyBlock. All rights reserved.
//

#import "OMBRenterApplicationAddModelViewController.h"

@interface OMBCreateListingAmenitiesViewController : 
  OMBRenterApplicationAddModelViewController
{
  NSArray *amenitiesArray;
}

@end
