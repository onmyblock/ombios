//
//  OMBCreateListingDatesViewController.h
//  OnMyBlock
//
//  Created by Tommy DANGerous on 12/13/13.
//  Copyright (c) 2013 OnMyBlock. All rights reserved.
//

#import "OMBRenterApplicationAddModelViewController.h"

@interface OMBCreateListingDatesViewController : 
  OMBRenterApplicationAddModelViewController
{
  NSDateFormatter *dateFormatter;
  UIDatePicker *datePicker;
  TextFieldPadding *selectedTextField;
}

@property (nonatomic, strong) TextFieldPadding *moveInDateTextField;
@property (nonatomic, strong) TextFieldPadding *moveOutDateTextField;

@end
