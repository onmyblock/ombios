//
//  OMBCreateListingLandlordViewController.m
//  OnMyBlock
//
//  Created by Tommy DANGerous on 12/16/13.
//  Copyright (c) 2013 OnMyBlock. All rights reserved.
//

#import "OMBCreateListingLandlordViewController.h"

@implementation OMBCreateListingLandlordViewController

#pragma mark - Initializer

- (id) init
{
  if (!(self = [super init])) return nil;

  self.screenName = self.title = @"Landlord Details";

  return self;
}

#pragma mark - Override

#pragma mark - Override UIViewController

- (void) loadView
{
  [super loadView];

  _emailTextField = [[TextFieldPadding alloc] init];
  _landlordNameTextField = [[TextFieldPadding alloc] init];
  _phoneTextField = [[TextFieldPadding alloc] init];

  textFieldArray = @[
    _landlordNameTextField,
    _emailTextField,
    _phoneTextField
  ];

  _emailTextField.keyboardType       = UIKeyboardTypeEmailAddress;
  _emailTextField.placeholder        = @"Email";
  _landlordNameTextField.placeholder = @"Full name";
  _phoneTextField.keyboardAppearance = UIKeyboardAppearanceDark;
  _phoneTextField.keyboardType       = UIKeyboardTypePhonePad;
  _phoneTextField.placeholder        = @"Phone";

  [self setFrameForTextFields];
}

- (void) viewWillAppear: (BOOL) animated
{
  [super viewWillAppear: animated];

  _emailTextField.text = self.delegate.residence.email;
  _landlordNameTextField.text = 
    [self.delegate.residence.landlordName capitalizedString];
  _phoneTextField.text = self.delegate.residence.phone;
}

#pragma mark - Methods

#pragma mark - Instance Methods

- (void) save
{
  self.delegate.residence.email = [_emailTextField.text lowercaseString];
  self.delegate.residence.landlordName = 
    [_landlordNameTextField.text lowercaseString];
  self.delegate.residence.phone = _phoneTextField.text;
  [self.navigationController popViewControllerAnimated: YES];
}

@end
