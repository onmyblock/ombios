//
//  OMBCreateListingDescriptionViewController.h
//  OnMyBlock
//
//  Created by Tommy DANGerous on 12/13/13.
//  Copyright (c) 2013 OnMyBlock. All rights reserved.
//

#import "OMBRenterApplicationAddModelViewController.h"

@interface OMBCreateListingDescriptionViewController : 
  OMBRenterApplicationAddModelViewController
<UITextViewDelegate>

@property (nonatomic, strong) UITextView *descriptionTextView;

@end
