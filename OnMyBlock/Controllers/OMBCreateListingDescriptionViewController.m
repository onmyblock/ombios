//
//  OMBCreateListingDescriptionViewController.m
//  OnMyBlock
//
//  Created by Tommy DANGerous on 12/13/13.
//  Copyright (c) 2013 OnMyBlock. All rights reserved.
//

#import "OMBCreateListingDescriptionViewController.h"

@implementation OMBCreateListingDescriptionViewController

#pragma mark - Initializer

- (id) init
{
  if (!(self = [super init])) return nil;

  self.screenName = self.title = @"Description";

  return self;
}

#pragma mark - Override

#pragma mark - Override UIViewController

- (void) loadView
{
  [super loadView];
  
  self.table.separatorStyle = UITableViewCellSeparatorStyleNone;
  self.table.tableHeaderView = nil;

  _descriptionTextView = [[UITextView alloc] init];

  textFieldArray = @[
    _descriptionTextView
  ];

  [self setFrameForTextViews];
}

- (void) viewDidAppear: (BOOL) animated
{
  [_descriptionTextView becomeFirstResponder];
}

- (void) viewWillAppear: (BOOL) animated
{
  [super viewWillAppear: animated];

  _descriptionTextView.text = self.delegate.residence.description;
}

#pragma mark - Protocol

#pragma mark - Protocol UITableViewDataSource

- (UITableViewCell *) tableView: (UITableView *) tableView
cellForRowAtIndexPath: (NSIndexPath *) indexPath
{
  UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:
    UITableViewCellStyleDefault reuseIdentifier: @"CellIdentifier"];
  cell.selectionStyle = UITableViewCellSelectionStyleNone;
  if (indexPath.section == 0) {
    if (indexPath.row == 0) {
      [cell.contentView addSubview: _descriptionTextView];
    }
  }
  return cell;
}

#pragma mark - Methods

#pragma mark - Instance Methods

- (void) save
{
  self.delegate.residence.description = _descriptionTextView.text;
  [self.navigationController popViewControllerAnimated: YES];
}

@end
