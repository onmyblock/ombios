//
//  OMBCreateListingDetailsViewController.m
//  OnMyBlock
//
//  Created by Tommy DANGerous on 12/13/13.
//  Copyright (c) 2013 OnMyBlock. All rights reserved.
//

#import "OMBCreateListingDetailsViewController.h"

@implementation OMBCreateListingDetailsViewController

#pragma mark - Initializer

- (id) init
{
  if (!(self = [super init])) return nil;

  propertyTypeArray = @[
    @"sublet",
    @"house",
    @"apartment"
  ];

  self.screenName = self.title = @"Details";

  return self;
}

#pragma mark - Override

#pragma mark - Override UIViewController

- (void) loadView
{
  [super loadView];

  _bathTextField         = [[TextFieldPadding alloc] init];
  _bedTextField          = [[TextFieldPadding alloc] init];
  _leaseMonthsTextField  = [[TextFieldPadding alloc] init];
  _propertyTypeTextField = [[TextFieldPadding alloc] init];
  _rentTextField         = [[TextFieldPadding alloc] init];
  textFieldArray = @[
    _rentTextField,
    _bedTextField,
    _bathTextField,
    _leaseMonthsTextField,
    _propertyTypeTextField
  ];

  _bathTextField.keyboardAppearance = 
    _bedTextField.keyboardAppearance = 
      _leaseMonthsTextField.keyboardAppearance = 
        _rentTextField.keyboardAppearance = UIKeyboardAppearanceDark;
  _bathTextField.keyboardType = UIKeyboardTypeDecimalPad;
  _bedTextField.keyboardType =
    _leaseMonthsTextField.keyboardType = UIKeyboardTypeNumberPad;
  _bathTextField.placeholder         = @"Baths (required)";
  _bedTextField.placeholder          = @"Beds (required)";
  _leaseMonthsTextField.placeholder  = @"Term of lease (required)";
  _propertyTypeTextField.placeholder = @"Property type (required)";
  _propertyTypeTextField.userInteractionEnabled = NO;
  _rentTextField.keyboardType        = UIKeyboardTypeDecimalPad;
  _rentTextField.placeholder         = @"Rent (required)";

  propertyTypePickerView = [[UIPickerView alloc] init];
  propertyTypePickerView.backgroundColor = [UIColor whiteColor];
  propertyTypePickerView.dataSource = self;
  propertyTypePickerView.delegate   = self;
  propertyTypePickerView.frame = CGRectMake(0.0f,
    self.view.frame.size.height,
      propertyTypePickerView.frame.size.width,
        propertyTypePickerView.frame.size.height);
  CALayer *topBorder = [CALayer layer];
  topBorder.backgroundColor = [UIColor grayLight].CGColor;
  topBorder.frame = CGRectMake(0.0f, 0.0f, 
    propertyTypePickerView.frame.size.width, 0.5f);
  [propertyTypePickerView.layer addSublayer: topBorder];
  propertyTypePickerView.showsSelectionIndicator = NO;
  [self.view addSubview: propertyTypePickerView];

  [self setFrameForTextFields];
}

- (void) viewWillAppear: (BOOL) animated
{
  [super viewWillAppear: animated];

  if (self.delegate.residence.bathrooms)
    _bathTextField.text = [NSString stringWithFormat: @"%.01f",
      self.delegate.residence.bathrooms];
  if (self.delegate.residence.bedrooms)
    _bedTextField.text = [NSString stringWithFormat: @"%.f",
      self.delegate.residence.bedrooms];
  if (self.delegate.residence.leaseMonths)
    _leaseMonthsTextField.text = [NSString stringWithFormat: @"%i",
      self.delegate.residence.leaseMonths];
  if (self.delegate.residence.propertyType)
    _propertyTypeTextField.text = 
      [self.delegate.residence.propertyType capitalizedString];
  if (self.delegate.residence.rent)
    _rentTextField.text = [NSString stringWithFormat: @"%.02f",
      self.delegate.residence.rent];

  NSUInteger index = [propertyTypeArray indexOfObjectPassingTest:
    ^(id obj, NSUInteger idx, BOOL *stop) {
      return [obj isEqualToString: self.delegate.residence.propertyType];
    }
  ];
  if (index != NSNotFound)
    [propertyTypePickerView selectRow: index inComponent: 0 animated: NO];
}

#pragma mark - Protocol

#pragma mark - Protocol UIPickerViewDataSource

- (NSInteger) numberOfComponentsInPickerView: (UIPickerView *) pickerView
{
  return 1;
}

- (NSInteger) pickerView: (UIPickerView *) pickerView 
numberOfRowsInComponent: (NSInteger) component
{
  return [propertyTypeArray count];
}

#pragma mark - Protocol UIPickerViewDelegate

- (void) pickerView: (UIPickerView *) pickerView didSelectRow: (NSInteger) row
inComponent: (NSInteger) component
{
  _propertyTypeTextField.text = 
    [[propertyTypeArray objectAtIndex: row] capitalizedString];
}

- (CGFloat) pickerView: (UIPickerView *) pickerView 
rowHeightForComponent: (NSInteger) component
{
  return 44.0f;
}

- (UIView *) pickerView: (UIPickerView *) pickerView viewForRow: (NSInteger) row
forComponent: (NSInteger) component reusingView: (UIView *) view
{
  UILabel *label = [[UILabel alloc] init];
  label.font = _propertyTypeTextField.font;
  label.frame = CGRectMake(20.0f, 0.0f, 
    self.view.frame.size.width - (20 * 2), 44.0f);
  label.text = [[propertyTypeArray objectAtIndex: row] capitalizedString];
  label.textColor = [UIColor textColor];
  return label;
}

#pragma mark - Protocol UITableViewDelegate

- (void) tableView: (UITableView *) tableView
didSelectRowAtIndexPath: (NSIndexPath *) indexPath
{
  if (indexPath.row == [textFieldArray indexOfObject: _propertyTypeTextField]) {
    if ([_propertyTypeTextField.text length] == 0)
      _propertyTypeTextField.text = 
        [[propertyTypeArray objectAtIndex: 0] capitalizedString];
    isEditing = YES;
    [self.view endEditing: YES];
    // Need to do this so the separator line doesn't disappear completely
    [tableView selectRowAtIndexPath: indexPath animated: NO
      scrollPosition: UITableViewScrollPositionNone];
    [tableView deselectRowAtIndexPath: indexPath animated: NO];
    [tableView reloadRowsAtIndexPaths: 
      @[[NSIndexPath indexPathForRow: 0 inSection: 1]] withRowAnimation:
        UITableViewRowAnimationNone];
    [UIView animateWithDuration: 0.25 animations: ^{
      propertyTypePickerView.frame = CGRectMake(0.0f,
        self.view.frame.size.height - propertyTypePickerView.frame.size.height,
          propertyTypePickerView.frame.size.width,
            propertyTypePickerView.frame.size.height);
    }];
    // [self.navigationItem setRightBarButtonItem: doneBarButtonItem
    //   animated: YES];
    [self.table scrollToRowAtIndexPath: indexPath 
      atScrollPosition: UITableViewScrollPositionTop animated: YES];
  }
}

#pragma mark - Protocol UITextFieldDelegate

- (void) textFieldDidBeginEditing: (UITextField *) textField
{
  [super textFieldDidBeginEditing: textField];
  [self hidePickerView];
}

#pragma mark - Methods

#pragma mark - Instance Methods

- (void) done
{
  isEditing = NO;
  [self.table beginUpdates];
  [self.table endUpdates];
  [self hidePickerView];
}

- (void) hidePickerView
{
  [UIView animateWithDuration: 0.25 animations: ^{
    propertyTypePickerView.frame = CGRectMake(0.0f,
      self.view.frame.size.height,
        propertyTypePickerView.frame.size.width,
          propertyTypePickerView.frame.size.height);
  }];
  // [self.navigationItem setRightBarButtonItem: saveBarButtonItem
  //   animated: YES];
}

- (void) save
{ 
  if ([_bathTextField.text length] > 0)
    self.delegate.residence.bathrooms = [_bathTextField.text floatValue];
  if ([_bedTextField.text length] > 0)
    self.delegate.residence.bedrooms = [_bedTextField.text floatValue];
  if ([_leaseMonthsTextField.text length] > 0)
    self.delegate.residence.leaseMonths = [_leaseMonthsTextField.text intValue];
  if ([_propertyTypeTextField.text length] > 0)
    self.delegate.residence.propertyType = 
      [_propertyTypeTextField.text lowercaseString];
  if ([_rentTextField.text length] > 0)
    self.delegate.residence.rent = [_rentTextField.text floatValue];
  [self.navigationController popViewControllerAnimated: YES];
}

@end
