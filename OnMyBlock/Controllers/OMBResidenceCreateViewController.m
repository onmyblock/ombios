//
//  OMBResidenceCreateViewController.m
//  OnMyBlock
//
//  Created by Tommy DANGerous on 12/12/13.
//  Copyright (c) 2013 OnMyBlock. All rights reserved.
//

#import "OMBResidenceCreateViewController.h"

#import "NSString+Extensions.h"
#import "OMBActivityView.h"
#import "OMBCreateListingAmenitiesViewController.h"
#import "OMBCreateListingDatesViewController.h"
#import "OMBCreateListingDescriptionViewController.h"
#import "OMBCreateListingDetailsViewController.h"
#import "OMBCreateListingLandlordViewController.h"
#import "OMBCreateListingLocationViewController.h"
#import "OMBCreateListingPicturesViewController.h"
#import "OMBResidence.h"
#import "OMBResidenceCreateConnection.h"
#import "UIColor+Extensions.h"

@implementation OMBResidenceCreateViewController

#pragma mark - Initializer

- (id) init
{
  if (!(self = [super init])) return nil;

  self.screenName = self.title = @"Create Listing";

  return self;
}

#pragma mark - Override

#pragma mark - Override UIViewController

- (void) loadView
{
  [super loadView];

  _residence = [[OMBResidence alloc] init];

  CGRect screen      = [[UIScreen mainScreen] bounds];
  float screenHeight = screen.size.height;

  self.view = [[UIView alloc] initWithFrame: screen];

  self.navigationItem.leftBarButtonItem = 
    [[UIBarButtonItem alloc] initWithTitle: @"Cancel" 
      style: UIBarButtonItemStylePlain target: self 
        action: @selector(cancel)];
  self.navigationItem.rightBarButtonItem = 
    [[UIBarButtonItem alloc] initWithTitle: @"Publish"
      style: UIBarButtonItemStylePlain target: self
        action: @selector(save)];

  table = [[UITableView alloc] initWithFrame: screen
    style: UITableViewStylePlain];
  table.alwaysBounceVertical = YES;
  table.backgroundColor = [UIColor colorWithWhite: 245/255.0f alpha: 1.0f];
  table.dataSource = self;
  table.delegate   = self;
  table.separatorInset = UIEdgeInsetsMake(0.0f, 20.0f, 0.0f, 0.0f);
  table.tableFooterView = [[UIView alloc] initWithFrame: 
    CGRectMake(0.0f, 0.0f, screenHeight, 44.0f)];
  table.tableFooterView.backgroundColor = [UIColor clearColor];
  table.tableHeaderView = [[UIView alloc] initWithFrame:
    table.tableFooterView.frame];
  table.tableHeaderView.backgroundColor = [UIColor clearColor];
  [self.view addSubview: table];

  activityView = [[OMBActivityView alloc] init];
  [self.view addSubview: activityView];
}

- (void) viewWillAppear: (BOOL) animated
{
  [super viewWillAppear: animated];

  [table reloadData];
}

#pragma mark - Protocol

#pragma mark - Protocol UITableViewDataSource

- (NSInteger) numberOfSectionsInTableView: (UITableView *) tableView
{
  return 1;
}

- (UITableViewCell *) tableView: (UITableView *) tableView
cellForRowAtIndexPath: (NSIndexPath *) indexPath
{
  static NSString *CellIdentifier = @"CellIdentifier";
  UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:
    CellIdentifier];
  if (!cell) 
    cell = [[UITableViewCell alloc] initWithStyle: UITableViewCellStyleValue1
      reuseIdentifier: CellIdentifier];
  cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
  cell.detailTextLabel.font = [UIFont fontWithName: @"HelveticaNeue-Light" 
    size: 15];
  cell.detailTextLabel.textColor = [UIColor grayMedium];
  cell.textLabel.font = cell.detailTextLabel.font;
  cell.textLabel.textColor = [UIColor textColor];
  if (indexPath.row == 0) {
    cell.detailTextLabel.text = [NSString stringWithFormat: @"%.f%%",
      [self picturesPercentage]];
    if ([self picturesPercentage] >= 100)
      cell.detailTextLabel.textColor = [UIColor green];
    else
      cell.detailTextLabel.textColor = [UIColor grayMedium];
    cell.textLabel.text = @"Pictures";
    CALayer *border = [CALayer layer];
    border.backgroundColor = [UIColor grayLight].CGColor;
    border.frame = CGRectMake(0.0f, 0.0f, cell.frame.size.width, 0.5f);
    [cell.layer addSublayer: border];
  }
  else if (indexPath.row == 1) {
    cell.detailTextLabel.text = [NSString stringWithFormat: @"%.f%%",
      [self locationPercentage]];
    if ([self locationPercentage] >= 100)
      cell.detailTextLabel.textColor = [UIColor green];
    else
      cell.detailTextLabel.textColor = [UIColor grayMedium];
    cell.textLabel.text = @"Location";
  }
  else if (indexPath.row == 2) {    
    cell.detailTextLabel.text = [NSString stringWithFormat: @"%.f%%",
      [self detailsPercentage]];
    if ([self detailsPercentage] >= 100)
      cell.detailTextLabel.textColor = [UIColor green];
    else
      cell.detailTextLabel.textColor = [UIColor grayMedium];
    cell.textLabel.text = @"Details";
  }
  else if (indexPath.row == 3) {
    cell.detailTextLabel.text = [NSString stringWithFormat: @"%.f%%",
      [self datesPercentage]];
    if ([self datesPercentage] >= 100)
      cell.detailTextLabel.textColor = [UIColor green];
    else
      cell.detailTextLabel.textColor = [UIColor grayMedium];
    cell.textLabel.text = @"Dates";
  }
  else if (indexPath.row == 4) {
    cell.detailTextLabel.text = [NSString stringWithFormat: @"%.f%%",
      [self descriptionPercentage]];
    if ([self descriptionPercentage] >= 100)
      cell.detailTextLabel.textColor = [UIColor green];
    else
      cell.detailTextLabel.textColor = [UIColor grayMedium];
    cell.textLabel.text = @"Description";    
  }
  else if (indexPath.row == 5)
    cell.textLabel.text = @"Amenities";
  else if (indexPath.row == 6)
    cell.textLabel.text = @"Landlord";
  if (indexPath.row == 
    [tableView numberOfRowsInSection: indexPath.section] - 1) {
    CALayer *border = [CALayer layer];
    border.backgroundColor = [UIColor grayLight].CGColor;
    border.frame = CGRectMake(0.0f, 44.0f - 0.5f, cell.frame.size.width, 0.5f);
    [cell.layer addSublayer: border];
  }
  return cell;
}

- (NSInteger) tableView: (UITableView *) tableView 
numberOfRowsInSection: (NSInteger) section
{
  return 7;
}

#pragma mark - Protocol UITableViewDelegate

- (void) tableView: (UITableView *) tableView
didSelectRowAtIndexPath: (NSIndexPath *) indexPath
{
  // Pictures
  if (indexPath.row == 0) {
    OMBCreateListingPicturesViewController *vc =
      [[OMBCreateListingPicturesViewController alloc] init];
    vc.delegate = self;
    [self.navigationController pushViewController: vc animated: YES];
  }
  // Location
  else if (indexPath.row == 1) {
    OMBCreateListingLocationViewController *vc = 
      [[OMBCreateListingLocationViewController alloc] init];
    vc.delegate = self;
    [self.navigationController pushViewController: vc animated: YES];
  }
  // Details
  else if (indexPath.row == 2) {
    OMBCreateListingDetailsViewController *vc = 
      [[OMBCreateListingDetailsViewController alloc] init];
    vc.delegate = self;
    [self.navigationController pushViewController: vc animated: YES];
  }
  // Dates
  else if (indexPath.row == 3) {
    OMBCreateListingDatesViewController *vc = 
      [[OMBCreateListingDatesViewController alloc] init];
    vc.delegate = self;
    [self.navigationController pushViewController: vc animated: YES];
  }
  // Description
  else if (indexPath.row == 4) {
    OMBCreateListingDescriptionViewController *vc =
      [[OMBCreateListingDescriptionViewController alloc] init];
    vc.delegate = self;
    [self.navigationController pushViewController: vc animated: YES];
  }
  // Amenities
  else if (indexPath.row == 5) {
    OMBCreateListingAmenitiesViewController *vc =
      [[OMBCreateListingAmenitiesViewController alloc] init];
    vc.delegate = self;
    [self.navigationController pushViewController: vc animated: YES];
  }
  // Landlord
  else if (indexPath.row == 6) {
    OMBCreateListingLandlordViewController *vc =
      [[OMBCreateListingLandlordViewController alloc] init];
    vc.delegate = self;
    [self.navigationController pushViewController: vc animated: YES];
  }
  [tableView deselectRowAtIndexPath: indexPath animated: YES];
}

- (CGFloat) tableView: (UITableView *) tableView 
heightForRowAtIndexPath: (NSIndexPath *) indexPath
{
  return 44.0f;
}

#pragma mark - Methods

#pragma mark - Instance Methods

- (void) cancel
{
  [self.navigationController dismissViewControllerAnimated: YES
    completion: ^{
      _residence = [[OMBResidence alloc] init];
      [activityView stopSpinning];
    }
  ];
}

- (CGFloat) datesPercentage
{
  int i = 0;
  if (_residence.moveInDate)
    i += 1;  
  return (i / 1.0) * 100.0f;
}

- (CGFloat) descriptionPercentage
{
  int i = 0;
  if ([[_residence.description stripWhiteSpace] length] > 0)
    i += 1;
  return (i / 1.0) * 100.0f;
}

- (CGFloat) detailsPercentage
{
  int i = 0;
  if (_residence.bathrooms)
    i += 1;
  if (_residence.bedrooms)
    i += 1;
  if (_residence.leaseMonths)
    i += 1;
  if (_residence.propertyType)
    i += 1;
  if (_residence.rent)
    i += 1;
  return (i / 5.0) * 100.0f;
}

- (CGFloat) locationPercentage
{
  int i = 0;
  if ([_residence.address length] > 0)
    i += 1;
  if ([_residence.city length] > 0)
    i += 1;
  if ([_residence.neighborhood length] > 0)
    i += 1;
  if ([_residence.state length] > 0)
    i += 1;
  return (i / 4.0) * 100.0f;
}

- (CGFloat) picturesPercentage
{
  CGFloat percentage = ([[_residence imagesArray] count] / 2.0) * 100.0f;
  return percentage > 100.0f ? 100.0f : percentage;
}

- (void) save
{
  NSString *message = @"";
  UIAlertView *alertView = [[UIAlertView alloc] initWithTitle: @"Incomplete"
    message: message delegate: nil
      cancelButtonTitle: @"OK" otherButtonTitles: nil];
  if ([self picturesPercentage] < 100) {
    message = @"Please add at least 2 pictures";
  }
  else if ([self locationPercentage] < 100) {
    message = @"Please fill in the location";
  }
  else if ([self detailsPercentage] < 100) {
    message = @"Please fill in the details";
  }
  else if ([self datesPercentage] < 100) {
    message = @"Please fill in the dates";
  }
  else if ([self descriptionPercentage] < 100) {
    message = @"Please fill in the description";
  }
  if ([message length] > 0) {
    alertView.message = message;
    [alertView show];
    return;
  }
  else {
    OMBResidenceCreateConnection *connection = 
      [[OMBResidenceCreateConnection alloc] initWithResidence: _residence];
    // connection.completionBlock = ^(NSError *error) {
    //   if (error) {
    //     [activityView stopSpinning];
    //     UIAlertView *alertView2 = [[UIAlertView alloc] initWithTitle: 
    //       @"Error" 
    //       message: error.localizedDescription delegate: nil
    //         cancelButtonTitle: @"Try again" otherButtonTitles: nil];
    //     [alertView2 show];
    //   }
    //   else {
    //     [self cancel];
    //   }
    // };
    [connection start];
    // [activityView startSpinning];
    [self cancel];
  }
}

@end
