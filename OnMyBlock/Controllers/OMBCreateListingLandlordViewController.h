//
//  OMBCreateListingLandlordViewController.h
//  OnMyBlock
//
//  Created by Tommy DANGerous on 12/16/13.
//  Copyright (c) 2013 OnMyBlock. All rights reserved.
//

#import "OMBRenterApplicationAddModelViewController.h"

@interface OMBCreateListingLandlordViewController : 
  OMBRenterApplicationAddModelViewController

@property (nonatomic, strong) TextFieldPadding *emailTextField;
@property (nonatomic, strong) TextFieldPadding *landlordNameTextField;
@property (nonatomic, strong) TextFieldPadding *phoneTextField;

@end
