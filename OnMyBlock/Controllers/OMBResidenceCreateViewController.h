//
//  OMBResidenceCreateViewController.h
//  OnMyBlock
//
//  Created by Tommy DANGerous on 12/12/13.
//  Copyright (c) 2013 OnMyBlock. All rights reserved.
//

#import "OMBViewController.h"

@class OMBActivityView;
@class OMBResidence;

@interface OMBResidenceCreateViewController : OMBViewController
<UITableViewDataSource, UITableViewDelegate>
{
  OMBActivityView *activityView;
  UITableView *table;
}

@property (nonatomic, strong) OMBResidence *residence;

@end
