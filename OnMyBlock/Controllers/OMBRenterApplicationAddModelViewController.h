//
//  OMBRenterApplicationAddModelViewController.h
//  OnMyBlock
//
//  Created by Tommy DANGerous on 12/6/13.
//  Copyright (c) 2013 OnMyBlock. All rights reserved.
//

#import "OMBTableViewController.h"

#import "OMBResidence.h"
#import "OMBResidenceCreateViewController.h"
#import "TextFieldPadding.h"
#import "UIColor+Extensions.h"

@class OMBResidenceCreateViewController;

@interface OMBRenterApplicationAddModelViewController : OMBTableViewController
<UITextFieldDelegate, UITextViewDelegate>
{
  UIBarButtonItem *cancelBarButtonItem;
  UIBarButtonItem *clearBarButtonItem;
  UIBarButtonItem *doneBarButtonItem;
  BOOL isEditing;
  UIBarButtonItem *saveBarButtonItem;
  NSArray *textFieldArray;
}

@property (nonatomic, weak) OMBResidenceCreateViewController *delegate;

#pragma mark - Methods

#pragma mark - Instance Methods

- (void) cancel;
- (void) clear;
- (void) done;
- (void) firstTextFieldBecomeFirstResponder;
- (void) save;
- (void) setFrameForTextFields;
- (void) setFrameForTextViews;
- (void) showCancelAndSaveBarButtonItems;
- (void) showClearAndDoneBarButtonItems;
- (id) validateFieldsInArray: (NSArray *) array;

@end
