//
//  OMBCreateListingDatesViewController.m
//  OnMyBlock
//
//  Created by Tommy DANGerous on 12/13/13.
//  Copyright (c) 2013 OnMyBlock. All rights reserved.
//

#import "OMBCreateListingDatesViewController.h"

#import "OMBRenterApplicationAddModelCell.h"

@implementation OMBCreateListingDatesViewController

#pragma mark - Initializer

- (id) init
{
  if (!(self = [super init])) return nil;

  dateFormatter = [NSDateFormatter new];
  dateFormatter.dateFormat = @"MMMM d, yyyy";

  self.screenName = self.title = @"Dates";

  return self;
}

#pragma mark - Override

#pragma mark - Override UIViewController

- (void) loadView
{
  [super loadView];
  
  _moveInDateTextField  = [[TextFieldPadding alloc] init];
  _moveOutDateTextField = [[TextFieldPadding alloc] init];

  textFieldArray = @[
    _moveInDateTextField, _moveOutDateTextField
  ];

  _moveInDateTextField.userInteractionEnabled =
    _moveOutDateTextField.userInteractionEnabled = NO;
  _moveInDateTextField.placeholder   = @"Required";
  _moveOutDateTextField.placeholder  = @"";
  _moveInDateTextField.textAlignment =
    _moveOutDateTextField.textAlignment = NSTextAlignmentRight;

  datePicker = [UIDatePicker new];
  datePicker.backgroundColor = [UIColor whiteColor];
  datePicker.calendar = [NSCalendar currentCalendar];
  datePicker.datePickerMode = UIDatePickerModeDate;
  datePicker.frame = CGRectMake(0.0f, 
    self.view.frame.size.height, 
      datePicker.frame.size.width, datePicker.frame.size.height);
  CALayer *topBorder = [CALayer layer];
  topBorder.backgroundColor = [UIColor grayLight].CGColor;
  topBorder.frame = CGRectMake(0.0f, 0.0f, 
    datePicker.frame.size.width, 0.5f);
  [datePicker.layer addSublayer: topBorder];
  datePicker.minimumDate = [NSDate date];
  [datePicker addTarget: self action: @selector(dateChanged)
    forControlEvents: UIControlEventValueChanged];
  [self.view addSubview: datePicker];

  [self setFrameForTextFields];
}

- (void) viewWillAppear: (BOOL) animated
{
  [super viewWillAppear: animated];

  if (self.delegate.residence.moveInDate)
    _moveInDateTextField.text = [dateFormatter stringFromDate:
      [NSDate dateWithTimeIntervalSince1970: 
        self.delegate.residence.moveInDate]];
  if (self.delegate.residence.moveOutDate)
    _moveOutDateTextField.text = [dateFormatter stringFromDate:
      [NSDate dateWithTimeIntervalSince1970: 
        self.delegate.residence.moveOutDate]];
}

#pragma mark - UITableViewDataSource

- (UITableViewCell *) tableView: (UITableView *) tableView
cellForRowAtIndexPath: (NSIndexPath *) indexPath
{
  OMBRenterApplicationAddModelCell *cell = 
    (OMBRenterApplicationAddModelCell *) [super tableView: tableView
      cellForRowAtIndexPath: indexPath];
  if (indexPath.section == 0) {
    float padding = 20.0f;

    UILabel *label = [[UILabel alloc] init];
    label.font      = _moveInDateTextField.font;
    label.textColor = _moveInDateTextField.textColor;
    CGRect rect = [@"Move Out Date" boundingRectWithSize:
      CGSizeMake(tableView.frame.size.width, 
        _moveInDateTextField.frame.size.height)
        options: NSStringDrawingUsesLineFragmentOrigin
          attributes: @{ NSFontAttributeName: _moveInDateTextField.font }
            context: nil];
    label.frame = CGRectMake(padding, 0, rect.size.width,
      _moveInDateTextField.frame.size.height);
    label.tag = 99;
    CGRect rect2 = _moveInDateTextField.frame;
    rect2.origin.x = label.frame.origin.x + label.frame.size.width + padding;
    rect2.size.width = tableView.frame.size.width - 
      (padding + label.frame.size.width + padding + padding);
    [cell.contentView addSubview: label];
    if (indexPath.row == 0) {
      label.text = @"Move In Date";
      _moveInDateTextField.frame = rect2;
    }
    else if (indexPath.row == 1) {
      label.text = @"Move Out Date";
      _moveOutDateTextField.frame = rect2;
    }
  }
  return cell;
}

#pragma mark - UITableViewDelegate

- (void) tableView: (UITableView *) tableView 
didDeselectRowAtIndexPath: (NSIndexPath *) indexPath
{
  UITableViewCell *cell = [tableView cellForRowAtIndexPath: indexPath];
  UILabel *label = (UILabel *) [cell.contentView viewWithTag: 99];
  label.textColor = _moveInDateTextField.textColor;
}

- (void) tableView: (UITableView *) tableView
didSelectRowAtIndexPath: (NSIndexPath *) indexPath
{
  selectedTextField = [textFieldArray objectAtIndex: indexPath.row];
  // Make the label color blue
  UITableViewCell *cell = [tableView cellForRowAtIndexPath: indexPath];
  UILabel *label = (UILabel *) [cell.contentView viewWithTag: 99];
  label.textColor = [UIColor blue];
  // Change all labels to normal color
  if ([selectedTextField.text length] > 0)
    [datePicker setDate: [dateFormatter dateFromString: selectedTextField.text]
      animated: YES];
  else
    selectedTextField.text = [dateFormatter stringFromDate: [NSDate date]];
  [UIView animateWithDuration: 0.25 animations: ^{
    datePicker.frame = CGRectMake(0.0f, 
      self.view.frame.size.height - datePicker.frame.size.height, 
        datePicker.frame.size.width, datePicker.frame.size.height);  
    }
  ];
  // [self.navigationItem setRightBarButtonItem: doneBarButtonItem
  //   animated: YES];
  [self.table scrollToRowAtIndexPath: indexPath 
    atScrollPosition: UITableViewScrollPositionTop animated: YES];
}

#pragma mark - Methods

#pragma mark - Instance Methods

- (void) dateChanged
{
  if (selectedTextField) {
    selectedTextField.text = [dateFormatter stringFromDate:
      [datePicker date]];
  }
}

- (void) done
{
  isEditing = NO;
  [self.table beginUpdates];
  [self.table endUpdates];
  // Deselect all rows
  for (int row = 0; row < [self.table numberOfRowsInSection: 0]; row++) {
    [self tableView: self.table didDeselectRowAtIndexPath: 
      [NSIndexPath indexPathForRow: row inSection: 0]];
  }
  [self hidePickerView];
}

- (void) hidePickerView
{
  [UIView animateWithDuration: 0.25 animations: ^{
    datePicker.frame = CGRectMake(0.0f, 
      self.view.frame.size.height, 
        datePicker.frame.size.width, datePicker.frame.size.height);
    }
  ];
  // [self.navigationItem setRightBarButtonItem: saveBarButtonItem
  //   animated: YES];
}

- (void) save
{  
  self.delegate.residence.moveInDate = 
    [[dateFormatter dateFromString: 
      _moveInDateTextField.text] timeIntervalSince1970];
  self.delegate.residence.moveOutDate = 
    [[dateFormatter dateFromString: 
      _moveOutDateTextField.text] timeIntervalSince1970];
  [self.navigationController popViewControllerAnimated: YES];
}

@end
