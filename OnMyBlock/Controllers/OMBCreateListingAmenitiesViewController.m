//
//  OMBCreateListingAmenitiesViewController.m
//  OnMyBlock
//
//  Created by Tommy DANGerous on 12/16/13.
//  Copyright (c) 2013 OnMyBlock. All rights reserved.
//

#import "OMBCreateListingAmenitiesViewController.h"

#import "UIColor+Extensions.h"

@implementation OMBCreateListingAmenitiesViewController

#pragma mark - Initializer

- (id) init
{
  if (!(self = [super init])) return nil;

  amenitiesArray = @[
    @"air conditioning",
    @"backyard",
    @"central heating",
    @"dishwasher",
    @"fence",
    @"front yard",
    @"garbage disposal",
    @"gym",
    @"hard floors",
    @"newly remodeled",
    @"patio/balcony",
    @"pool",
    @"storage",
    @"washer/dryer" 
  ];

  self.screenName = self.title = @"Amenities";

  return self;
}

#pragma mark - Override

#pragma mark - Override UIViewController

- (void) viewWillAppear: (BOOL) animated
{
  [super viewWillAppear: animated];


}

#pragma mark - Protocol

#pragma mark - Protocol UITableViewDataSource

- (UITableViewCell *) tableView: (UITableView *) tableView
cellForRowAtIndexPath: (NSIndexPath *) indexPath
{
  static NSString *CellIdentifier = @"CellIdentifier";
  UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:
    CellIdentifier];
  if (!cell)
    cell = [[UITableViewCell alloc] initWithStyle: UITableViewCellStyleDefault
      reuseIdentifier: CellIdentifier];
  if (indexPath.section == 0) {
    NSString *key = [[amenitiesArray objectAtIndex: 
      indexPath.row] lowercaseString];
    cell.textLabel.font = [UIFont fontWithName: @"HelveticaNeue-Light" 
      size: 15];
    cell.textLabel.text = [key capitalizedString];
    cell.textLabel.textColor = [UIColor textColor];
    // Check for checkmarks or not
    if ([[self.delegate.residence.amenities objectForKey: key] intValue] == 0)
      cell.accessoryType = UITableViewCellAccessoryNone;
    else
      cell.accessoryType = UITableViewCellAccessoryCheckmark;
    // Borders
    CGRect screen = [[UIScreen mainScreen] bounds];
    // Top border
    if (indexPath.row == 0) {
      CALayer *topBorder = [CALayer layer];
      topBorder.backgroundColor = self.table.separatorColor.CGColor; 
      topBorder.frame = CGRectMake(0.0f, 0.0f, screen.size.width, 0.5f);
      [cell.contentView.layer addSublayer: topBorder];
    }
    // Bottom border
    else if (indexPath.row == [amenitiesArray count] - 1) {
      CALayer *bottomBorder = [CALayer layer];
      bottomBorder.backgroundColor = self.table.separatorColor.CGColor;
      bottomBorder.frame = CGRectMake(0.0f, 44.0f - 0.5f,
        screen.size.width, 0.5f);
      [cell.contentView.layer addSublayer: bottomBorder];
    }
  }
  return cell;
}

- (NSInteger) tableView: (UITableView *) tableView
numberOfRowsInSection: (NSInteger) section
{
  if (section == 0)
    return [amenitiesArray count];
  return 0;
}

#pragma mark - Protocol UITableViewDelegate

- (void) tableView: (UITableView *) tableView
didSelectRowAtIndexPath: (NSIndexPath *) indexPath
{
  UITableViewCell *cell = [tableView cellForRowAtIndexPath: indexPath];
  int value = 0;
  NSString *key = [[amenitiesArray objectAtIndex: 
    indexPath.row] lowercaseString];
  if (cell.accessoryType == UITableViewCellAccessoryCheckmark)
    cell.accessoryType = UITableViewCellAccessoryNone;
  else {
    cell.accessoryType = UITableViewCellAccessoryCheckmark;
    value = 1;
  }
  [self.delegate.residence.amenities setObject: [NSNumber numberWithInt: value]
    forKey: key];
  [tableView deselectRowAtIndexPath: indexPath animated: YES];
}

- (CGFloat) tableView: (UITableView *) tableView
heightForRowAtIndexPath: (NSIndexPath *) indexPath
{
  if (indexPath.section == 0) {
    return 44.0f;
  }
  else if (indexPath.section == 1) {
    if (isEditing)
      return 216.0f;
  }
  return 0.0f;
}

@end
