//
//  OMBCreateListingLocationViewController.m
//  OnMyBlock
//
//  Created by Tommy DANGerous on 12/13/13.
//  Copyright (c) 2013 OnMyBlock. All rights reserved.
//

#import "OMBCreateListingLocationViewController.h"

#import "OMBGoogleMapsReverseGeocodingConnection.h"
#import "UIImage+Resize.h"

@implementation OMBCreateListingLocationViewController

#pragma mark - Initializer

- (id) init
{
  if (!(self = [super init])) return nil;

  locationManager                 = [[CLLocationManager alloc] init];
  locationManager.delegate        = self;
  locationManager.desiredAccuracy = kCLLocationAccuracyBest;
  locationManager.distanceFilter  = 50;

  neighborhoodArray = @[
    @"downtown",
    @"hillcrest",
    @"la jolla",
    @"mission beach",
    @"mission valley",
    @"ocean beach",
    @"pacific beach",
    @"university towne center"    
  ];

  self.screenName = self.title = @"Location";

  return self;
}

#pragma mark - Override

#pragma mark - Override UIViewController

- (void) loadView
{
  [super loadView];

  _addressTextField      = [[TextFieldPadding alloc] init];
  _cityTextField         = [[TextFieldPadding alloc] init];
  _neighborhoodTextField = [[TextFieldPadding alloc] init];
  _stateTextField        = [[TextFieldPadding alloc] init];
  _unitTextField         = [[TextFieldPadding alloc] init];
  _zipTextField          = [[TextFieldPadding alloc] init];

  textFieldArray = @[
    _addressTextField,
    _unitTextField,
    _cityTextField,
    _stateTextField,
    _zipTextField,
    _neighborhoodTextField
  ];

  // Address
  _addressTextField.placeholder = @"Address (required)";
  UIButton *button = [[UIButton alloc] init];
  button.backgroundColor = [UIColor whiteColor];
  button.frame = CGRectMake(0.0f, 0.0f, 24.0f * 3, 24.0f);
  button.layer.borderColor  = [UIColor blue].CGColor;
  button.layer.borderWidth  = 1.0f;
  button.layer.cornerRadius = 2.0f;
  // [button setImage: [UIImage image: [UIImage imageNamed: @"gps_cursor_blue.png"]
  //   size: CGSizeMake(20.0f, 20.0f)] forState: UIControlStateNormal];
  button.titleLabel.font = [UIFont fontWithName: @"HelveticaNeue-Light" 
    size: 11];
  [button setTitle: @"Use Current" forState: UIControlStateNormal];
  [button setTitleColor: [UIColor blue]
    forState: UIControlStateNormal];
  [button addTarget: self action: @selector(useCurrentLocation)
    forControlEvents: UIControlEventTouchUpInside];
  _addressTextField.rightView = button;
  _addressTextField.rightViewMode = UITextFieldViewModeAlways;

  _cityTextField.placeholder         = @"City (required)";
  _neighborhoodTextField.placeholder = @"Neighborhood (required)";
  _neighborhoodTextField.userInteractionEnabled = NO;
  _stateTextField.placeholder        = @"State (required)";
  _unitTextField.keyboardAppearance  = UIKeyboardAppearanceDark;
  _unitTextField.keyboardType        = UIKeyboardTypeNumberPad;
  _unitTextField.placeholder         = @"Unit #";
  _zipTextField.keyboardType         = UIKeyboardTypeNumberPad;
  _zipTextField.keyboardAppearance   = UIKeyboardAppearanceDark;
  _zipTextField.placeholder          = @"Zip";

  neighborhoodPickerView = [[UIPickerView alloc] init];
  neighborhoodPickerView.backgroundColor = [UIColor whiteColor];
  neighborhoodPickerView.dataSource = self;
  neighborhoodPickerView.delegate   = self;
  neighborhoodPickerView.frame = CGRectMake(0.0f,
    self.view.frame.size.height,
      neighborhoodPickerView.frame.size.width,
        neighborhoodPickerView.frame.size.height);
  CALayer *topBorder = [CALayer layer];
  topBorder.backgroundColor = [UIColor grayLight].CGColor;
  topBorder.frame = CGRectMake(0.0f, 0.0f, 
    neighborhoodPickerView.frame.size.width, 0.5f);
  [neighborhoodPickerView.layer addSublayer: topBorder];
  neighborhoodPickerView.showsSelectionIndicator = NO;
  [self.view addSubview: neighborhoodPickerView];

  [self setFrameForTextFields];
}

- (void) viewWillAppear: (BOOL) animated
{
  [super viewWillAppear: animated];

  _addressTextField.text = [self.delegate.residence.address capitalizedString];
  _cityTextField.text = [self.delegate.residence.city capitalizedString];
  _neighborhoodTextField.text = 
    [self.delegate.residence.neighborhood capitalizedString];
  _stateTextField.text = [self.delegate.residence.state capitalizedString];
  if (self.delegate.residence.unit)
    _unitTextField.text = [NSString stringWithFormat: @"%i",
      self.delegate.residence.unit];
  _zipTextField.text = self.delegate.residence.zip;

  NSUInteger index = [neighborhoodArray indexOfObjectPassingTest:
    ^(id obj, NSUInteger idx, BOOL *stop) {
      return [obj isEqualToString: self.delegate.residence.neighborhood];
    }
  ];
  if (index != NSNotFound)
    [neighborhoodPickerView selectRow: index inComponent: 0 animated: NO];
}

#pragma mark - Protocol

#pragma mark - Protocol CLLocationManagerDelegate

- (void) locationManager: (CLLocationManager *) manager
didFailWithError: (NSError *) error
{
  NSLog(@"Location manager did fail with error: %@", 
    error.localizedDescription);
}

- (void) locationManager: (CLLocationManager *) manager
didUpdateLocations: (NSArray *) locations
{
  CLLocationCoordinate2D coordinate;
  if ([locations count]) {
    for (CLLocation *location in locations) {
      coordinate = location.coordinate;
    }
    OMBGoogleMapsReverseGeocodingConnection *connection =
      [[OMBGoogleMapsReverseGeocodingConnection alloc] initWithCoordinate:
        coordinate];
    connection.delegate = self;
    [connection start];
  }
  [locationManager stopUpdatingLocation];
}

#pragma mark - Protocol UIPickerViewDataSource

- (NSInteger) numberOfComponentsInPickerView: (UIPickerView *) pickerView
{
  return 1;
}

- (NSInteger) pickerView: (UIPickerView *) pickerView 
numberOfRowsInComponent: (NSInteger) component
{
  return [neighborhoodArray count];
}

#pragma mark - Protocol UIPickerViewDelegate

- (void) pickerView: (UIPickerView *) pickerView didSelectRow: (NSInteger) row
inComponent: (NSInteger) component
{
  _neighborhoodTextField.text = 
    [[neighborhoodArray objectAtIndex: row] capitalizedString];
}

- (CGFloat) pickerView: (UIPickerView *) pickerView 
rowHeightForComponent: (NSInteger) component
{
  return 44.0f;
}

- (UIView *) pickerView: (UIPickerView *) pickerView viewForRow: (NSInteger) row
forComponent: (NSInteger) component reusingView: (UIView *) view
{
  UILabel *label = [[UILabel alloc] init];
  label.font = _neighborhoodTextField.font;
  label.frame = CGRectMake(20.0f, 0.0f, 
    self.view.frame.size.width - (20 * 2), 44.0f);
  label.text = [[neighborhoodArray objectAtIndex: row] capitalizedString];
  label.textColor = [UIColor textColor];
  return label;
}

#pragma mark - Protocol UITableViewDelegate

- (void) tableView: (UITableView *) tableView
didSelectRowAtIndexPath: (NSIndexPath *) indexPath
{
  if (indexPath.row == [textFieldArray indexOfObject: _neighborhoodTextField]) {
    if ([_neighborhoodTextField.text length] == 0)
      _neighborhoodTextField.text = 
        [[neighborhoodArray objectAtIndex: 0] capitalizedString];
    isEditing = YES;
    [self.view endEditing: YES];
    // Need to do this so the separator line doesn't disappear completely
    [tableView selectRowAtIndexPath: indexPath animated: NO
      scrollPosition: UITableViewScrollPositionNone];
    [tableView deselectRowAtIndexPath: indexPath animated: NO];
    [tableView reloadRowsAtIndexPaths: 
      @[[NSIndexPath indexPathForRow: 0 inSection: 1]] withRowAnimation:
        UITableViewRowAnimationNone];
    [UIView animateWithDuration: 0.25 animations: ^{
      neighborhoodPickerView.frame = CGRectMake(0.0f,
        self.view.frame.size.height - neighborhoodPickerView.frame.size.height,
          neighborhoodPickerView.frame.size.width,
            neighborhoodPickerView.frame.size.height);
    }];
    // [self.navigationItem setRightBarButtonItem: doneBarButtonItem
    //   animated: YES];
    [self.table scrollToRowAtIndexPath: indexPath 
      atScrollPosition: UITableViewScrollPositionTop animated: YES];
  }
}

#pragma mark - Protocol UITextFieldDelegate

- (void) textFieldDidBeginEditing: (UITextField *) textField
{
  [super textFieldDidBeginEditing: textField];
  [self hidePickerView];
}

#pragma mark - Methods

#pragma mark - Instance Methods

- (void) done
{
  isEditing = NO;
  [self.table beginUpdates];
  [self.table endUpdates];
  [self hidePickerView];
}

- (void) hidePickerView
{
  [UIView animateWithDuration: 0.25 animations: ^{
    neighborhoodPickerView.frame = CGRectMake(0.0f,
      self.view.frame.size.height,
        neighborhoodPickerView.frame.size.width,
          neighborhoodPickerView.frame.size.height);
  }];
  // [self.navigationItem setRightBarButtonItem: saveBarButtonItem
  //   animated: YES];
}

- (void) save
{
  self.delegate.residence.address = [_addressTextField.text lowercaseString];
  self.delegate.residence.city = [_cityTextField.text lowercaseString];
  self.delegate.residence.neighborhood = 
    [_neighborhoodTextField.text lowercaseString];
  self.delegate.residence.state = [_stateTextField.text lowercaseString];
  if ([_unitTextField.text length] > 0)
    self.delegate.residence.unit = [_unitTextField.text intValue];
  self.delegate.residence.zip = _zipTextField.text;
  [self.navigationController popViewControllerAnimated: YES];
}

- (void) useCurrentLocation
{
  [locationManager startUpdatingLocation];
}

@end
