//
//  OMBCreateListingPicturesViewController.m
//  OnMyBlock
//
//  Created by Tommy DANGerous on 12/16/13.
//  Copyright (c) 2013 OnMyBlock. All rights reserved.
//

#import "OMBCreateListingPicturesViewController.h"

#import "OMBResidenceImage.h"

@implementation OMBCreateListingPicturesViewController

#pragma mark - Initializer

- (id) init
{
  if (!(self = [super init])) return nil;

  self.screenName = self.title = @"Pictures";

  return self;
}

#pragma mark - Override

#pragma mark - Override UIViewController

- (void) loadView
{
  [super loadView];

  self.navigationItem.rightBarButtonItem = 
    [[UIBarButtonItem alloc] initWithTitle: @"Add"
      style: UIBarButtonItemStylePlain target: self
        action: @selector(add)];

  self.table.editing         = YES;
  self.table.separatorStyle  = UITableViewCellSeparatorStyleNone;
  self.table.tableFooterView = [[UIView alloc] initWithFrame: CGRectZero];
  self.table.tableHeaderView = nil;

  // Action sheet
  photoActionSheet = [[UIActionSheet alloc] initWithTitle: nil 
    delegate: self cancelButtonTitle: @"Cancel" destructiveButtonTitle: nil 
      otherButtonTitles: @"Take Photo", @"Choose Existing", nil];
  [self.view addSubview: photoActionSheet];
}

- (void) viewWillAppear: (BOOL) animated
{
  [super viewWillAppear: animated];

  [self.table reloadData];
}

#pragma mark - Protocol

#pragma mark - Protocol UIActionSheetDelegate

- (void) actionSheet: (UIActionSheet *) actionSheet 
clickedButtonAtIndex: (NSInteger) buttonIndex
{
  // Image picker controller
  UIImagePickerController *imagePickerController = 
    [[UIImagePickerController alloc] init];
  imagePickerController.allowsEditing = YES;
  imagePickerController.delegate = self;
  if (buttonIndex == 0) {
    if ([UIImagePickerController isSourceTypeAvailable: 
      UIImagePickerControllerSourceTypeCamera]) {

      imagePickerController.sourceType = 
        UIImagePickerControllerSourceTypeCamera;
    }
    else {
      imagePickerController.sourceType = 
        UIImagePickerControllerSourceTypePhotoLibrary;
    }
  }
  else if (buttonIndex == 1) {
    imagePickerController.sourceType = 
      UIImagePickerControllerSourceTypePhotoLibrary;
  }
  if (buttonIndex == 0 || buttonIndex == 1) {
    [self presentViewController: imagePickerController animated: YES
      completion: nil];
  }
}

#pragma mark - Protocol UIImagePickerControllerDelegate

- (void) imagePickerController: (UIImagePickerController *) picker 
didFinishPickingMediaWithInfo: (NSDictionary *) info
{
  UIImage *image = [info objectForKey: UIImagePickerControllerEditedImage];
  if (!image)
    image = [info objectForKey: UIImagePickerControllerOriginalImage];

  int position = 0;
  if ([[self.delegate.residence imagesArray] count] > 0) {
    OMBResidenceImage *residenceImage = 
      [[self.delegate.residence imagesArray] objectAtIndex: 
        [[self.delegate.residence imagesArray] count] - 1];
    position = residenceImage.position + 1;
  }
  NSDateFormatter *dateFormatter = [NSDateFormatter new];
  dateFormatter.dateFormat = @"yyyy-MM-d HH.mm.ss";
  [self.delegate.residence addImage: image atPosition: position
    withString: [dateFormatter stringFromDate: [NSDate date]]];

  // Hide the image picker
  [picker dismissViewControllerAnimated: YES completion: ^{
    [self.table reloadData];
  }];
}

#pragma mark - Protocol UITableViewDataSource

- (UITableViewCell *) tableView: (UITableView *) tableView
cellForRowAtIndexPath: (NSIndexPath *) indexPath
{
  static NSString *CellIdentifier = @"CellIdentifier";
  UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:
    CellIdentifier];
  if (!cell)
    cell = [[UITableViewCell alloc] initWithStyle: UITableViewCellStyleDefault
      reuseIdentifier: CellIdentifier];
  cell.selectionStyle = UITableViewCellSelectionStyleNone;
  if (indexPath.section == 0) {
    OMBResidenceImage *residenceImage = 
      [[self.delegate.residence imagesArray] objectAtIndex: indexPath.row];
    cell.imageView.image = residenceImage.image;
  }
  return cell;
}

- (void) tableView: (UITableView *) tableView 
commitEditingStyle: (UITableViewCellEditingStyle) editingStyle 
forRowAtIndexPath: (NSIndexPath *) indexPath
{
  if (editingStyle == UITableViewCellEditingStyleDelete) {
    [tableView beginUpdates];
    [tableView deleteRowsAtIndexPaths: @[indexPath] 
      withRowAnimation: UITableViewRowAnimationFade];
    OMBResidenceImage *residenceImage = 
      [[self.delegate.residence imagesArray] objectAtIndex: indexPath.row];
    [self.delegate.residence.images removeObject: residenceImage];
    // Update positions
    for (OMBResidenceImage *image in [self.delegate.residence imagesArray]) {
      image.position = [[self.delegate.residence imagesArray] indexOfObject: 
        image];
    }
    [tableView endUpdates];
  }
}

- (void) tableView: (UITableView *) tableView 
moveRowAtIndexPath: (NSIndexPath *) fromIndexPath 
toIndexPath: (NSIndexPath *) toIndexPath
{
  OMBResidenceImage *residenceImage = 
    [[self.delegate.residence imagesArray] objectAtIndex: fromIndexPath.row];
  if (fromIndexPath.row > toIndexPath.row) {
    NSIndexSet *indexSet = 
      [NSIndexSet indexSetWithIndexesInRange: 
        NSMakeRange(toIndexPath.row, fromIndexPath.row - toIndexPath.row)];
    for (OMBResidenceImage *object in 
      [[self.delegate.residence imagesArray] objectsAtIndexes: indexSet]) {

      object.position += 1;
    }
  }
  else if (fromIndexPath.row < toIndexPath.row) {
    NSIndexSet *indexSet = 
      [NSIndexSet indexSetWithIndexesInRange:
        NSMakeRange(fromIndexPath.row + 1, 
          toIndexPath.row - fromIndexPath.row)];
    for (OMBResidenceImage *object in
      [[self.delegate.residence imagesArray] objectsAtIndexes: indexSet]) {

      object.position -= 1;
    }
  }
  residenceImage.position = toIndexPath.row;
}

- (NSInteger) tableView: (UITableView *) tableView
numberOfRowsInSection: (NSInteger) section
{
  if (section == 0)
    return [[self.delegate.residence imagesArray] count];
  else if (section == 1)
    return 1;
  return 0;
}

#pragma mark - Protocol UITableViewDelegate

- (UITableViewCellEditingStyle) tableView: (UITableView *) tableView 
editingStyleForRowAtIndexPath: (NSIndexPath *) indexPath
{
  if (indexPath.section == 0) {
    return UITableViewCellEditingStyleDelete;
  }
  return UITableViewCellEditingStyleNone;
}

- (CGFloat) tableView: (UITableView *) tableView
heightForRowAtIndexPath: (NSIndexPath *) indexPath
{
  if (indexPath.section == 0) {
    return 20.0f + 60.0f + 20.0f;
  }
  return 0.0f;
}

#pragma mark - Methods

#pragma mark - Instance Methods

- (void) add
{
  [photoActionSheet showInView: self.view];
}

@end
